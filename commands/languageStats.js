import * as conf from 'conf'
import * as chalk from 'chalk'
import fs from 'fs';
import * as Path from 'path'
import { exit } from 'process';

/**
 * Medel class for Report, As we grow we can seperate out in different model folder
 */
export class Report {
    constructor(summery,results){
        this.summery=summery;
        this.results=results;
    }
}

/**
 * To analyse languageStats and console log report
 * @param {string} path folder path which is target of languageStats analysis
 */
export async function languageStats (path) {
    let results=[]
    const freqMap = new Map();
    const stats = new Object();
    
    const onError = (e)=>{
        console.log("Something went wrong.")
        // console.log(e);
        exit();
    }

    await processFiles(path,results,onError);
    
    results.forEach(file =>{
        let freq=freqMap.has(file.language)?freqMap.get(file.language)+1:1;
        freqMap.set(file.language,freq);
    })
    const totalFiles = results.length;
    freqMap.forEach((numOfFiles,language)=> stats[language]=(numOfFiles/totalFiles).toFixed(6));
    const finalReport = new Report(stats,results);
    console.log(finalReport);
    return finalReport;
}



/**
 * function to  calculate the area of a rectangle
 * @param {string} path - The path of the folder
 * @param {Array<{path: number, language: string}>} results 
 * @param {Function} onError - The function to call on Error
 * @returns {number} - The area of the rectangle
 */

 async function processFiles(dirname,results,onError) {
    try{
        let filenames =  (fs.readdirSync(dirname))
        filenames.forEach(function(filename) {
            const fullPath = Path.join(dirname, filename);
            
            //  if(fs.statSync(fullPath).isDirectory() && filename!=="node_modules" )
             if(fs.statSync(fullPath).isDirectory())
                    processFiles(fullPath,results);
             else
             {
                 let language = getLanguage(filename);
                 if(language!==""){
                    results.push({
                        "path":fullPath,
                        "language": language
                    })
                 }
             }
                 
           }); 
    }catch(err){
       if(onError){
           onError(err);
           
       }
       
    }
}

/**
* 
* Returns language of the file based on it's extension
* @param {string} filename - The path of the file
* @returns {string} - Returns the  language of ther file
* In future we can add support for more languages if needed
 */
function getLanguage(filename) {
    const extension = filename.split('.').pop();
    switch (extension){
        case 'py':
            return "python"
        case 'js':
            return "js"
        case 'MakeFile':
            return "make"
        default:
            return ""
    }
}
