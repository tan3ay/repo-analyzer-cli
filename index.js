#! /usr/bin/env node
//const { program } = require('commander')
import { program } from "commander";
import {languageStats} from "./commands/languageStats.js";

//const languageStats = require('./commands/languageStats')

program
    .command('cl <path>')   
    .description('Analyse the provided repository and returns information on languages used in the repository.')
    .action(languageStats)

program
    .command('check-language <path>')   
    .description('Analyse the provided repository and returns information on languages used in the repository.')
    .action(languageStats)
    

program.parse()
