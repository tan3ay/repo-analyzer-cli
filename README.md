#### repo-analyzer-cli

This project provides cli commands to analyse any folder/repository to extract useful information regarding codebase.

# How to use?

# 1. For development & enhancement

- Clone repo from git@gitlab.com:tan3ay/repo-analyzer-cli.git
- Run
  - npm install
  - npm i -g
- This will install the cli in your machine
- To execute cli

  - Go to repo
  - Use
    - repo-cli cl <path>
    - repo-cli check-language <path>
  - e.g.
    - repo-cli cl .
    - repo-cli cl test

- Use jenkins shell and pipeline for build and deploy respectively

# Build

Plese check
./JenkinsFileBuild.sh

# Deploy

Please check
./JenkinsFile

========================================================================+

# 2. Use utility with docker

- Tool can be used just by downloading docker image
- Steps:-

  - docker pull tan3ay/assignment:latest
  - docker run -it -e FOLDER_PATH={sub_folder} -e COMMAND={command} -v $(pwd):/app/target tan3ay/assignment
    e.g.

    - docker run -it -e FOLDER_PATH=test -e COMMAND=cl -v $(pwd):/app/target tan3ay/assignment

          - FOLDER_PATH = Path within current folder/repo
          - Default FOLDER_PATH = Current folder
          - COMMAND :- Command to run on folder/repo
          - Default COMMAND = check-language

  e.g. This Will run check-language command in current folder.

  - docker run -it -v $(pwd):/app/target tan3ay/assignment

# In future we can add more commands to extract different types of information
