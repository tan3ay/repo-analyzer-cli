FROM node:14
# Taking COMMAND from user so that in future we could extend it to perform variety of other analysis operation on repo
ENV COMMAND="cl"

# FOLDER_PATH inside current directory if user wants to perform the command on specific child folder
# Default is current folder where user is executing the docker run
ENV FOLDER_PATH="."

WORKDIR /app
COPY package.json /app
RUN npm install

COPY . /app
RUN npm i -g

CMD ["sh", "-c", "cd target && repo-cli ${COMMAND} ${FOLDER_PATH}"]
