
let path = require('path');
let exec = require('child_process').exec;
const { EOL } = require('os');

test('Execute On Test Directory', async () => {
  let result = await  cli(['check-language', 'test'], '.');
  expect(result.code).toBe(0);
  let expectedRes = [
    "Report {",
    "  summery: { js: '0.500000', python: '0.500000' },",
    "  results: [",
    "    { path: 'test/jsFile.js', language: 'js' },",
    "    { path: 'test/test.py', language: 'python' }",
    "  ]",
    "}"
  ]
  
 expect(result.stdout.trim().split(EOL)).toStrictEqual(expectedRes);
})

test('Execute without path', async () => {
    let result = await  cli(['check-language', ''], '.');
    expect(result.code).toBe(1);
    let expectedRes = "error: missing required argument 'path'\n";
    expect(result.stderr).toBe(expectedRes);
  })


function cli(args, cwd) {

  return new Promise(resolve => { 
    exec(`node ${path.resolve('./index')} ${args.join(' ')}`,
    { cwd }, 
    (error, stdout, stderr) => { 
        // console.log(stdout);
        resolve({
    code: error && error.code ? error.code : 0,
    error,
    stdout,
    stderr })
  })
})}